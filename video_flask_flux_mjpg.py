#!flask/bin/python
from flask import Flask, render_template, Response
from flask import request
import os, re

app = Flask(__name__)

def get_chunk(byte1=None, byte2=None):
    full_path = "zoo.mp4"
    file_size = os.stat(full_path).st_size
    start = 0
    
    if byte1 < file_size:
        start = byte1
    if byte2:
        length = byte2 + 1 - byte1
    else:
        length = file_size - start

    with open(full_path, 'rb') as f:
        f.seek(start)
        chunk = f.read(length)
    return chunk, start, length, file_size


def get_chunk2(byte1=None, byte2=None):
    full_path2 = "voitures.mp4"
    file_size = os.stat(full_path2).st_size
    start = 0
    
    if byte1 < file_size:
        start = byte1
    if byte2:
        length = byte2 + 1 - byte1
    else:
        length = file_size - start

    with open(full_path2, 'rb') as f:
        f.seek(start)
        chunk2 = f.read(length)
    return chunk2, start, length, file_size

def get_chunk3(byte1=None, byte2=None):
    full_path3 = "output.mp4"
    file_size = os.stat(full_path3).st_size
    start = 0
    
    if byte1 < file_size:
        start = byte1
    if byte2:
        length = byte2 + 1 - byte1
    else:
        length = file_size - start

    with open(full_path3, 'rb') as f:
        f.seek(start)
        chunk3 = f.read(length)
    return chunk3, start, length, file_size


@app.route('/video/zoo')
def get_file():
    range_header = request.headers.get('Range', None)
    byte1, byte2 = 0, None
    if range_header:
        match = re.search(r'(\d+)-(\d*)', range_header)
        groups = match.groups()

        if groups[0]:
            byte1 = int(groups[0])
        if groups[1]:
            byte2 = int(groups[1])
       
    chunk, start, length, file_size = get_chunk(byte1, byte2)
    resp = Response(chunk, 206, mimetype='video/mjpg',
                      content_type='video/mjpg', direct_passthrough=True)
    resp.headers.add('Content-Range', 'bytes {0}-{1}/{2}'.format(start, start + length - 1, file_size))
    return resp


@app.route('/video/voitures')
def get_file2():
    range_header = request.headers.get('Range', None)
    byte1, byte2 = 0, None
    if range_header:
        match = re.search(r'(\d+)-(\d*)', range_header)
        groups = match.groups()

        if groups[0]:
            byte1 = int(groups[0])
        if groups[1]:
            byte2 = int(groups[1])
       
    chunk2, start, length, file_size = get_chunk2(byte1, byte2)
    resp = Response(chunk2, 206, mimetype='video/mjpg',
                      content_type='video/mjpg', direct_passthrough=True)
    resp.headers.add('Content-Range', 'bytes {0}-{1}/{2}'.format(start, start + length - 1, file_size))
    return resp

@app.route('/video/output')
def get_file3():
    range_header = request.headers.get('Range', None)
    byte1, byte2 = 0, None
    if range_header:
        match = re.search(r'(\d+)-(\d*)', range_header)
        groups = match.groups()

        if groups[0]:
            byte1 = int(groups[0])
        if groups[1]:
            byte2 = int(groups[1])
       
    chunk3, start, length, file_size = get_chunk3(byte1, byte2)
    resp = Response(chunk3, 206, mimetype='video/mp4v',
                      content_type='video/mp4v', direct_passthrough=True)
    resp.headers.add('Content-Range', 'bytes {0}-{1}/{2}'.format(start, start + length - 1, file_size))
    return resp






@app.route('/')
def index():
    return "Hello, World!"

@app.after_request
def after_request(response):
    response.headers.add('Accept-Ranges', 'bytes')
    return response

if __name__ == '__main__':
    app.run(host="127.0.0.1",port=8888,debug=True,threaded=True)
